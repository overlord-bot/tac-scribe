# Database Setup

See example instructions for ubuntu here [here](https://kitcharoenp.github.io/postgresql/postgis/2018/05/28/set_up_postgreSQL_postgis.html)

# Database Migrations

Run migrations using

`sequel -m db/ postgres://username:password@database_ip:database_port/tac_scribe`

