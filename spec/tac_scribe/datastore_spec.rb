# frozen_string_literal: true

require_relative '../spec_helper'
require_relative '../../lib/tac_scribe/datastore'

class MockDatastore < TacScribe::Datastore
  def mock(result)
    @db = Sequel.connect('mock://postgres')

    @db.fetch = result
  end
end

describe TacScribe::Datastore do
  let(:datastore) { Class.new(MockDatastore).instance }

  describe 'default configuration' do
    { host: 'localhost',
      port: '5432',
      database: 'tac_scribe',
      username: 'tac_scribe',
      password: 'tac_scribe' }.each_pair do |field, value|
      describe "for #{field}" do
        it 'is correct' do
          expect(value).to eq datastore.configuration.send(field)
        end
      end
    end
  end

  describe '#configure' do
    %i[host port database username password].each do |field|
      it "saves #{field}" do
        datastore.configure do |config|
          config.public_send("#{field}=", field.to_s)
        end

        expect(field.to_s).to eq datastore.configuration.send(field)
      end
    end
  end

  # We cannot unit test connecting to the Postgresql database so this
  # has to remain untested
  # describe '#connect' do; end;
end
