# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'tac_scribe/version'

puts "RUBY_PLATFORM = #{RUBY_PLATFORM}"

Gem::Specification.new do |spec|
  spec.name          = 'tac_scribe'
  spec.version       = TacScribe::VERSION
  spec.authors       = ['Jeffrey Jones']
  spec.email         = ['jeff@jones.be']

  spec.summary       = 'Write Tacview data to PostGIS database'
  spec.description   = 'Write Tacview data to PostGIS database'
  spec.homepage      = 'https://gitlab.com/overlord-bot/tac_scribe'
  spec.license       = 'AGPL-3.0-or-later'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.metadata['yard.run'] = 'yri'

  spec.platform = RUBY_PLATFORM == 'java' ? 'java' : 'ruby'

  if RUBY_PLATFORM == 'java'
    spec.add_dependency 'activerecord-jdbcpostgresql-adapter'
  else
    spec.add_dependency 'pg', '~>1.1'
    spec.add_development_dependency 'pry-byebug', '~>3.9'
  end

  spec.add_dependency 'georuby', '~>2.5'
  spec.add_dependency 'sequel', '~>5.22'
  # The following gem is newish and not heavily updated so the chances
  # of an API breaking change are higher then normal. Therefore lock the
  # version
  spec.add_dependency 'concurrent-ruby', '~>1.1.5'
  spec.add_dependency 'haversine', '~>0.3'
  spec.add_dependency 'sequel-postgis-georuby', '0.1.2'
  spec.add_dependency 'tacview_client', '~>0.1'
  spec.add_dependency 'cinc', '~>0.1'

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.8'
  spec.add_development_dependency 'rubocop', '~>0.73'
  spec.add_development_dependency 'simplecov', '~>0.17'
  spec.add_development_dependency 'yard', '~>0.9'
end
