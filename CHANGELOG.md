# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Fixed
- Airfields retrieved using CinC will skip position localization

## [0.8.0] - 2021-01-09
### Changed
- Remove airfield loading from json files
- CinC is now mandatory

### Fixed
- Aerodromes (including oilrigs... ) no longer have special position handling.
  This is in relation to the removal of airfields from json files and requiring
  CinC. Side effect is oilrigs will now be positioned correctly.

## [0.7.6] - 2020-08-13
### Changed
- Fix the default port number

## [0.7.5] - 2020-08-12
### Changed
- Various fixes related to airfield population using CinC

## [0.7.4] - 2020-08-10
### Changed
- Fix typo that was causing all objects to be registered as airbases

## [0.7.3] - 2020-08-10
### Changed
- Bug fixes after adding CinC support

## [0.7.2] - 2020-08-10
### Added
- Support for "Commander-In-Chief" to get airbase information

## [0.7.1] - 2020-08-10
### Changed
- Fixed issue where "name" fields were not being populated
  if airfields were not being loaded first

## [0.7.0] - 2020-07-5
### Changed
- Synced Lat/Lon calculations with DCS and fixed issue where
  calculations were incorrect if only one value was updated

## [0.6.2] - 2020-04-19
### Changed
- Fixed typo causing app to fail

## [0.6.1] - 2020-04-19
### Changed
- Test build for gem issue

## [0.6.0] - 2020-04-19
### Added
- Calculates and stores speed of units

### Fixed
- Made more robust against failures and more logging messages

## [0.5.0] - 2020-03-17
### Changed
- Added missing db column to migration file
- Clear the cache on server restart
- Add more info to logging

## [0.4.0] - 2020-03-13
### Changed
- Switch to periodic writing to the database

## [0.3.0] - 2019-10-27
### Changed
- Changed the command-line name to match gem

### Fixed
- Sleep between connection attempts

### Added
- Unit type filtering

## [0.2.1] - 2019-10-13
### Changed
- Added separate events in / events out for the queue logging

## [0.2.0] - 2019-10-13
### Added
- JRuby support

### Changed
- Improve logging of events with number of events processed.


## [0.1.1] - 2019-09-05
### Added
- Added Net::HTTP error handling

